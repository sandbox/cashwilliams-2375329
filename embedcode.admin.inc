<?php

/**
 * Generates the embedcode editing form.
 */
function embedcode_form($form, &$form_state, $embedcode, $op = 'edit') {
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $embedcode->description,
  );
  $form['code'] = array(
    '#type' => 'textarea',
    '#title' => t('Code'),
    '#default_value' => $embedcode->code,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Embed Code'),
    '#weight' => 40,
  );
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function embedcode_form_submit(&$form, &$form_state) {
  $embedcode = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  $embedcode->save();
  $form_state['redirect'] = EMBEDCODE_URI_PATH;
}